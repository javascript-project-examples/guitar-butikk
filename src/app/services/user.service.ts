import { Injectable } from '@angular/core';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private _user?:User = {userName:"",guitarCart:[]}

  constructor() { }

  set userName(value:string){
    this._user!.userName = value  }

    get userName():string{
      return this._user!.userName
    }

    get user():User{
      return this._user!
    }
}
