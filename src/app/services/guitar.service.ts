import { Injectable } from "@angular/core";
import { Guitar } from "../models/guitar";
import { Observable } from 'rxjs'
import { HttpClient } from "@angular/common/http";

@Injectable({ providedIn: 'root' })
export class GuitarService {

    constructor(private readonly http: HttpClient) {

    }


    getGuitars(): Observable<Guitar[]> {

        return this.http.get<Guitar[]>('https://juniper-western-direction.glitch.me/guitars')   
    }

    getGuitarById(guitarId:string) {
        return this.http.get<Guitar>(`https://juniper-western-direction.glitch.me/guitars/${guitarId}`)  
    }



}