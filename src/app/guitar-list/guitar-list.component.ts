import { HttpClient } from "@angular/common/http";
import { Component, OnInit } from "@angular/core";
import { EROFS } from "constants";
import { Guitar } from "../models/guitar";
import {Router} from '@angular/router'
import { GuitarService } from "../services/guitar.service";
import { UserService } from "../services/user.service";
import { User } from "../models/user";


@Component({
  selector: "app-guitar-list",
  templateUrl: './guitar-list.component.html',
  styleUrls: ['guitar-list.component.css']
})
export class GuitarListComponent implements OnInit {

  constructor(private readonly guitarService:GuitarService, private readonly router:Router, private readonly userService:UserService) { }

  guitars: Guitar[] = [{
    id: "916E597F-C629-4306-BD31-BBF767D9A8AA",
    model: "Telecaster Thinline Original '60s",
    manufacturer: "Fender",
    bodyType: "Semi-Hollow",
    materials: {
      neck: "Maple",
      fretboard: "Maple (Coated)",
      body: "Mahogany"
    },
    strings: 6,
    image: "https://www.fmicassets.com/Damroot/ZoomJpg/10001/0110172834_gtr_frt_001_rr.jpg"
  }]


  ngOnInit(): void {
    // get guitar data and set in local state
    this.guitarService.getGuitars()
    .subscribe(
      (response) => {
        this.guitars = response
      },
      (error) => {
        console.log(error)
      }
    )
  }


  handleGuitarSelected(guitarId:string){

   this.router.navigate(['details',guitarId])


  }

  get user():User{
    return this.userService.user
  }

}