import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Guitar } from '../models/guitar';
import { User } from '../models/user';
import { GuitarService } from '../services/guitar.service';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-guitar-details',
  templateUrl: './guitar-details.component.html',
  styleUrls: ['./guitar-details.component.css']
})
export class GuitarDetailsComponent implements OnInit {
  currentGuitarId?:string = ''
  currentGuitar?:Guitar
  constructor(private readonly route:ActivatedRoute,private readonly guitarService: GuitarService, private readonly userService:UserService) { }

  ngOnInit(): void {
    this.currentGuitarId = String(this.route.snapshot.paramMap.get('guitarId'))
    this.guitarService.getGuitarById(this.currentGuitarId)
    .subscribe(
      (response) => {
        this.currentGuitar = response
      },
      (error) => {
        console.log(error)
      }
    )
    
  }
  get user():User{
    return this.userService.user
  }

}
