import { Routes,RouterModule } from "@angular/router";
import { GuitarDetailsPage } from "./pages/guitar-details/guitar-details.page";
import { GuitarListPage } from "./pages/guitar-list/guitar-list.page";
import { LoginPage } from "./pages/login/login.page";
import {NgModule} from '@angular/core'
import { LoginGuardGuard } from "./guards/login-guard.guard";

const routes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        component: GuitarListPage
    },
    {
        path: 'login',
        component: LoginPage,
        canActivate:[LoginGuardGuard]
    },
    {
        path: 'details/:guitarId',
        component: GuitarDetailsPage
    }
]

@NgModule({
imports:[RouterModule.forRoot(routes)],
exports:[RouterModule]
})
export class AppRoutingModule{}