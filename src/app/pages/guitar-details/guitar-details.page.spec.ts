import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GuitarDetailsPage } from './guitar-details.page';

describe('GuitarDetailsPage', () => {
  let component: GuitarDetailsPage;
  let fixture: ComponentFixture<GuitarDetailsPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GuitarDetailsPage ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GuitarDetailsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
