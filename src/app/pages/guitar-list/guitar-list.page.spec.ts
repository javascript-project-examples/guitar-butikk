import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GuitarListPage } from './guitar-list.page';

describe('GuitarListPage', () => {
  let component: GuitarListPage;
  let fixture: ComponentFixture<GuitarListPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GuitarListPage ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GuitarListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
