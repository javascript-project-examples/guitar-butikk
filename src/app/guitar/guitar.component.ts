import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Guitar } from '../models/guitar';

@Component({
  selector: 'app-guitar',
  templateUrl: './guitar.component.html',
  styleUrls: ['./guitar.component.css']
})
export class GuitarComponent implements OnInit {

  constructor() { }

  @Input() guitar?: Guitar
  @Output() guitarSelected: EventEmitter<string> = new EventEmitter()

  ngOnInit(): void {


  }

  signalGuitarSelected() {

    this.guitarSelected.emit(this.guitar?.id)

  }

}
