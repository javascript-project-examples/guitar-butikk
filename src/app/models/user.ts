import { Guitar } from "./guitar"


export interface User {
    userName: string,
    guitarCart:Guitar[]
}