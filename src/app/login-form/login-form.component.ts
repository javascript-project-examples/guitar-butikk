import { Component, OnInit } from '@angular/core';
import { User } from '../models/user';
import { UserService } from '../services/user.service';
import {NgForm} from '@angular/forms'
import {  Router } from '@angular/router';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {

  constructor(private readonly userService:UserService, private readonly router:Router) { }

  ngOnInit(): void {
  }

  login(form:NgForm){

    this.userService.userName = form.value.userName
    this.router.navigate([''])

  }

}
