import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { GuitarListComponent } from './guitar-list/guitar-list.component';
import { GuitarComponent } from './guitar/guitar.component';
import { GuitarListPage } from './pages/guitar-list/guitar-list.page';
import { LoginPage } from './pages/login/login.page';
import { GuitarDetailsPage } from './pages/guitar-details/guitar-details.page';
import { AppRoutingModule } from './app-routing.module';
import {HttpClientModule} from '@angular/common/http';
import { GuitarDetailsComponent } from './guitar-details/guitar-details.component';
import { LoginFormComponent } from './login-form/login-form.component'
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    GuitarListComponent,
    GuitarComponent,
    LoginPage,
    GuitarListPage,
    GuitarDetailsPage,
    GuitarDetailsComponent,
    LoginFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
